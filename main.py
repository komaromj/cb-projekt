import sys
from antlr4 import *
from SPLLexer import SPLLexer
from SPLParser import SPLParser
from SPLVisitor import SPLVisitor
import logging

# Startpunkt des Programms, auszuführende SPL'-Programme können als Argument übergeben werden. Beispiel: py main.py /examples/Mid.spl 
def main(argv):
    input_stream = FileStream(argv[1])# der Filestream gibt den Inhalt des ersten Kommandozeilenarguments,
    lexer = SPLLexer(input_stream)    # der Lexer zerlegt diesen in Tokens
    stream = CommonTokenStream(lexer)
    parser = SPLParser(stream)        # die vom Parser in einen Syntaxbaum mit Kontextinformaitonen konvertiert werden,
    tree = parser.startProgram()
    visitor = SPLVisitor()
    visitor.visit(tree)               # der anschließend vom Visitor durchlaufen wird.


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.DEBUG)
    main(sys.argv)
