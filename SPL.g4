grammar SPL;

startProgram
    : (declaration)* EOF
    ;

declaration
    : varDecl
    | statement
    ;

varDecl
    : VAR IDENTIFIER (EQUALS expression)? SEMI
    ;

statement
    : exprStmt
    | ifStmt
    | printStmt
    | whileStmt
    | block
    ;

exprStmt
    : expression SEMI
    ;

ifStmt
    : IF LPAREN expression RPAREN statement (ELSE statement)?
    ;

printStmt
    : PRINT expression SEMI
    ;

whileStmt
    : WHILE LPAREN expression RPAREN statement
    ;

block
    : LCURLY (declaration)* RCURLY
    ;

expression
    : assignment
    ;

assignment
    : IDENTIFIER EQUALS assignment
    | logic_or
    ;

logic_or
    : logic_and (OR logic_and)*
    ;

logic_and
    : equality (AND equality)*
    ;

equality
    : comparison ((LOGNEQ | LOGEQ) comparison)*
    ;

comparison
    : term ((GT | GEQ | LT | LEQ) term)*
    ;

term
    : factor ((MINUS | PLUS) factor)*
    ;

factor
    : unary ((SLASH | STAR) unary)*
    ;

unary
    : (NOT | MINUS) unary
    | primary
    ;

primary
    : TRUE
    | FALSE
    | NUMBER
    | STRING
    | LPAREN expression RPAREN
    | IDENTIFIER
    ;

// Keywords
TRUE    : 'true';
FALSE   : 'false';
AND     : 'and';
OR      : 'or';
VAR     : 'var';
PRINT   : 'print';
IF      : 'if';
ELSE    : 'else';
WHILE   : 'while';

// Special characters
SEMI    : ';';
LPAREN  : '(';
RPAREN  : ')';
LCURLY  : '{';
RCURLY  : '}';

STRING  : '"' ~["]* '"';

NUMBER  : [0-9]+ ('.' [0-9]+)?;

IDENTIFIER  : [A-Za-z][A-Za-z0-9]*;

// Operators
PLUS    : '+';
MINUS   : '-';
STAR    : '*';
SLASH   : '/';
EQUALS  : '=';
LOGEQ   : '==';
LOGNEQ  : '!=';
LT      : '<';
GT      : '>';
LEQ     : '<=';
GEQ     : '>=';
NOT     : '!';

WS      : [ \t\r\n]+ -> skip;
COMMENT : '//' ~[\r\n]* '\r'? '\n' -> skip;