# Compilerbau -- Projekt SoSe 2023

## Beschreibung

In diesem Repo ist unsere Umsetzung des Compilerbau Pojektes des Sommersemesters 2023 zu finden, realisiert wurde ein Parser und ein simpler Interpreter für die SPL'-Sprache in Python3 mithilfe von ANTLR. Die Grammatik der Sprache kann in [SPL.g4](https://gitlab.informatik.hu-berlin.de/komaromj/cb-projekt/-/blob/main/SPL.g4) gefunden werden. Die meisten Dateien wurden von [ANTLR](https://www.antlr.org/index.html) generiert, mit Anpassungen in [main.py](https://gitlab.informatik.hu-berlin.de/komaromj/cb-projekt/-/blob/main/main.py) und der wesentlichen Interpreter-Logik im [Visitor](https://gitlab.informatik.hu-berlin.de/komaromj/cb-projekt/-/blob/main/gen/SPLVisitor.py). Im Visitor ist der größte Teil des selbstgeschriebenen Codes zu finden, da dieser nicht vollständig aus der Grammatik heraus erzeugt werden kann.

Entscheidende Features unserer SPL'-Sprache und beispielhafte Referenzen zu Zeilennummern sind in der [SEMANTICS.md](https://gitlab.informatik.hu-berlin.de/komaromj/cb-projekt/-/blob/main/SEMANTICS.md) Datei erläutert.

Im Ordner [examples](https://gitlab.informatik.hu-berlin.de/komaromj/cb-projekt/-/tree/main/examples) sind SPL' Programme zu finden, die beispielsweise zum Testen verwendet werden können.


## Benötigte Packete

Benötigte Pyhton-Packete wurden der [ANTLR-Installationsbeschreibung](https://github.com/antlr/antlr4/blob/master/doc/getting-started.md) entnommen:

    pip install antlr4-tools
    pip install antlr4-python3-runtime
    pip install splparser

## Verwendung

Auszuführende SPL'-Programme werden als Kommandozeilenargument an main.py übergeben. Beispiel:

    main.py /examples/Mid.spl

Je nach System ist es unter Umständen notwendig zuerst die Dateien main.py und enviroment.py in den gen Ordner zu verschieben, und main.py aus eben diesem Ordner heraus zu starten.
    
## Tests

Wir haben unser Programm mit den in [/examples](https://gitlab.informatik.hu-berlin.de/komaromj/cb-projekt/-/tree/main/examples) gegebenen Beispielen Euclidean, Fibonacci und Mid getestet und eine gute Ausgabe erhalten.

## Authoren 

[Julian Komaromy](https://gitlab.informatik.hu-berlin.de/komaromj),
[Emilia Oleynik](https://gitlab.informatik.hu-berlin.de/oleynike),
[Tim Lindstädt](https://gitlab.informatik.hu-berlin.de/lindstat)

<!-- ## License

## Project status -->
