# Generated from /home/julian/PycharmProjects/spl/SPL.g4 by ANTLR 4.12.0
import logging
from environment import Environment, TypedValue

from antlr4 import *

if __name__ is not None and "." in __name__:
    from .SPLParser import SPLParser
else:
    from SPLParser import SPLParser

default_unsuccessful_error_msg = "unable to interpret program..."


# This class defines a complete generic visitor for a parse tree produced by SPLParser.


def semantic_error(msg, ctx):
    """

    Display an error message with a line number and exit the program

    :param msg: error message
    :param ctx: context of error
    """
    # retrieve line number from context
    line_number = ctx.start.line
    logging.error(f"semantic error at line {line_number}: {msg}")
    exit(default_unsuccessful_error_msg)


def get_typed_value(x, t):
    if x.inferred_type == t:
        return x.value
    else:
        return None


class SPLVisitor(ParseTreeVisitor):
    environment = Environment()

    # Visit a parse tree produced by SPLParser#startProgram.
    def visitStartProgram(self, ctx: SPLParser.StartProgramContext):
        return self.visitChildren(ctx)

    # Visit a parse tree produced by SPLParser#declaration.
    def visitDeclaration(self, ctx: SPLParser.DeclarationContext):
        return self.visitChildren(ctx)

    # Visit a parse tree produced by SPLParser#varDecl.
    def visitVarDecl(self, ctx: SPLParser.VarDeclContext):
        # IDENTIFIER can be turned into a string representation of the variable name
        variable_name = str(ctx.IDENTIFIER())

        if ctx.expression() is not None:
            # evaluate the expression to find out what value we set to
            expr_result = self.visitExpression(ctx.expression())
            logging.debug(f"set '{variable_name} = {expr_result.value}' {expr_result.inferred_type}")
        else:
            # we only declare, but not actually initialized the variable
            expr_result = None
            logging.debug(f"declaring but not initializing '{variable_name}'")

        try:
            self.environment.declare_new_variable(variable_name, expr_result)
        except LookupError:
            semantic_error(f"variable {variable_name} has already been declared in this scope", ctx)

    # Visit a parse tree produced by SPLParser#statement.
    def visitStatement(self, ctx: SPLParser.StatementContext):
        return self.visitChildren(ctx)

    # Visit a parse tree produced by SPLParser#exprStmt.
    def visitExprStmt(self, ctx: SPLParser.ExprStmtContext):
        self.visitExpression(ctx.expression())

    # Visit a parse tree produced by SPLParser#ifStmt.
    def visitIfStmt(self, ctx: SPLParser.IfStmtContext):
        var = self.visitExpression(ctx.expression())
        if var.inferred_type is bool:
            if var.value:
                logging.info("condition of if statement is true")
                return self.visitStatement(ctx.statement(0))
            elif ctx.ELSE() is not None:
                logging.info("condition of if statement is false, going into else")
                return self.visitStatement(ctx.statement(1))
        else:
            semantic_error(f"condition of if statement must be a boolean", ctx)

    # Visit a parse tree produced by SPLParser#printStmt.
    def visitPrintStmt(self, ctx: SPLParser.PrintStmtContext):
        expr = self.visitExpression(ctx.expression())
        print(expr.value)

    # Visit a parse tree produced by SPLParser#whileStmt.
    def eval_boolean_expression(self, expr_context):
        expr = self.visitExpression(expr_context)
        if expr.inferred_type is bool:
            return expr.value
        else:
            semantic_error(f"expression '{expr_context.getText()}' is not bool", expr_context)

    def visitWhileStmt(self, ctx: SPLParser.WhileStmtContext):
        # while -> WHILE LPAREN expression RPAREN statement
        max_iterations = 10000
        iterations = 0

        condition = self.eval_boolean_expression(ctx.expression())

        while condition and iterations < max_iterations:
            logging.info("executing statement inside while loop")
            self.visitStatement(ctx.statement())
            condition = self.eval_boolean_expression(ctx.expression())
            logging.debug(f"environment after statement: {self.environment}")
            iterations += 1

        if iterations == max_iterations:
            logging.error("loop reached maximum iteration count")
            exit(-1)

        logging.info("exiting while loop")

    # Visit a parse tree produced by SPLParser#block. This never returns any value.
    def visitBlock(self, ctx: SPLParser.BlockContext):
        self.environment.enter_new_scope()

        # call the children to execute side effects (e.g. printing or modifying environment)
        self.visitChildren(ctx)

        self.environment.exit_scope()

    # Visit a parse tree produced by SPLParser#expression.
    def visitExpression(self, ctx: SPLParser.ExpressionContext):
        return self.visitAssignment(ctx.assignment())

    # Visit a parse tree produced by SPLParser#assignment.
    def visitAssignment(self, ctx: SPLParser.AssignmentContext):
        if ctx.assignment() is not None:
            variable_name = str(ctx.IDENTIFIER())
            value = self.visitAssignment(ctx.assignment())

            try:
                self.environment.update_declared_var(variable_name, value)
                logging.debug(f"assigned {value} to '{variable_name}'")
            except TypeError:
                semantic_error(f"variable {variable_name} has a type "
                               f"incompatible with {value.inferred_type}", ctx)
            except KeyError:
                semantic_error(f"variable {variable_name} hasn't been declared", ctx)

            return value
        else:
            # print("logic_or assignment")
            return self.visitLogic_or(ctx.logic_or())

    # Visit a parse tree produced by SPLParser#logic_or.
    def visitLogic_or(self, ctx: SPLParser.Logic_orContext):
        # checking logic_or -> logic_and (OR logic_and)*

        if len(ctx.logic_and()) >= 2:
            children = [self.visitLogic_and(x) for x in ctx.logic_and()]

            # if we actually want to use or statements, all children must be booleans
            if not all([c.inferred_type == bool for c in children]):
                semantic_error("only booleans may be compared using OR", ctx)

            result = children[0].value
            for x in children[1:]:
                result = result or x.value

            return TypedValue(value=result, inferred_type=bool)
        else:
            return self.visitLogic_and(ctx.logic_and(0))

    # Visit a parse tree produced by SPLParser#logic_and.
    def visitLogic_and(self, ctx: SPLParser.Logic_andContext):
        # checking Logic_and -> equality (AND equality)+

        assert isinstance(ctx.equality(), list)

        equality_values = [self.visitEquality(x) for x in ctx.equality()]

        if len(equality_values) == 1:
            return equality_values[0]
        else:
            # if there are (n + 1) boolean values in the list, we know that
            # we will perform exactly n AND operations - no need to check for them (done by parser)
            if all([x.inferred_type == bool for x in equality_values]):
                return TypedValue(value=all([x.values for x in equality_values]),
                                  inferred_type=bool)
            else:
                semantic_error("AND only operates on boolean values", ctx)

    # Visit a parse tree produced by SPLParser#equality.
    def visitEquality(self, ctx: SPLParser.EqualityContext):
        # checking Equality -> comparison ((Logneq|logeq) comparison)*

        if len(ctx.comparison()) >= 2:
            children = [self.visitComparison(x) for x in ctx.comparison()]
            result = children[0]

            for i in range(1, len(children)):
                next_tv = children[i]

                if next_tv.inferred_type != result.inferred_type:
                    semantic_error("logical comparisons can only be used on values of the same type")

                # extract the operator from the context
                operator = ctx.getChild(2 * i - 1)
                if operator.getSymbol().text == "==":
                    logging.debug("found equality is comparison == comparison")
                    result = TypedValue(result.value == next_tv.value, inferred_type=bool)
                elif operator.getSymbol().text == "!=":
                    logging.debug("found equality is comparison != comparison")
                    result = TypedValue(result.value != next_tv.value, inferred_type=bool)
                else:
                    raise AssertionError("unexpected: operator is neither LOGEQ nor LOGNEQ")

            return result
        # checking Equality -> comparison
        else:
            return self.visitComparison(ctx.comparison(0))

    # Visit a parse tree produced by SPLParser#comparison.
    def visitComparison(self, ctx: SPLParser.ComparisonContext):
        # checking comparison -> term ((GT|GEQ|LT|LEQ) term)* , but
        # if there is more than two terms, it compares bool with a number -> sematic error

        assert isinstance(ctx.term(), list)

        values = [self.visitTerm(x) for x in ctx.term()]
        if len(values) == 1:
            # in this case, term should only be a single value
            return values[0]
        elif len(values) == 2:
            # basic comparison of two float values
            left_term = values[0]
            right_term = values[1]

            # GT, GEQ, LT, LEQ only for float
            if not (left_term.inferred_type is float and right_term.inferred_type is float):
                semantic_error("only number values can be compared", ctx)

            operator = ctx.getChild(1).getSymbol().text
            # checking which operator is used
            if operator == ">":
                logging.debug("found comparison is float > float")
                comparison_result = left_term.value > right_term.value
            elif operator == ">=":
                logging.debug("found comparison is float >= float")
                comparison_result = left_term.value >= right_term.value
            elif operator == "<":
                logging.debug("found comparison is float < float")
                comparison_result = left_term.value < right_term.value
            elif operator == "<=":
                logging.debug("found comparison is float <= float")
                comparison_result = left_term.value <= right_term.value
            else:
                raise AssertionError("unexpected state in long branch of comparison")

            return TypedValue(value=comparison_result, inferred_type=bool)
        else:
            semantic_error("only binary comparisons between numerical values are supported", ctx)

    # Visit a parse tree produced by SPLParser#term.
    def visitTerm(self, ctx: SPLParser.TermContext):
        # checking Term -> factor ((MINUS|PLUS) factor)*,
        assert isinstance(ctx.factor(), list)

        factors = [self.visitFactor(x) for x in ctx.factor()]

        if len(factors) == 1:
            return factors[0]
        else:
            result = factors[0]

            for i in range(1, len(factors)):
                next_factor = factors[i]
                operator = ctx.getChild(2 * i - 1)

                if operator.getSymbol().text == "-":
                    logging.debug("TERM: float - float")
                    if result.inferred_type == float and next_factor.inferred_type == float:
                        result = TypedValue(result.value - next_factor.value, float)
                    else:
                        semantic_error("minus operation is only supported on number types", ctx)
                elif operator.getSymbol().text == "+":
                    logging.debug("TERM: float + float")
                    if result.inferred_type == float and next_factor.inferred_type == float:
                        result = TypedValue(result.value + next_factor.value, float)
                    elif result.inferred_type == str and next_factor.inferred_type == str:
                        result = TypedValue(result.value + next_factor.value, str)
                    else:
                        semantic_error("minus operation is only supported on number and string types", ctx)
                else:
                    raise AssertionError("unknown operator encountered in factor")
            return result

    # Visit a parse tree produced by SPLParser#factor.
    def visitFactor(self, ctx: SPLParser.FactorContext):
        # checking factor -> unary ((Slash|Star) unary)+
        assert isinstance(ctx.unary(), list)

        children = [self.visitUnary(u) for u in ctx.unary()]
        logging.debug(f"FACTOR: found {len(children)} unary-subexpressions")

        if len(children) == 1:
            return children[0]
        else:
            if not all([x.inferred_type is float for x in children]):
                semantic_error("multiplication / division is only supported for number types", ctx)

            # a TypedValue with type float that accumulates the value of the expression
            result = children[0]

            for i in range(1, len(children)):
                unary_tv = children[i]
                operator = ctx.getChild(2 * i - 1)

                # checking Slash/Star, pay attention on the order of the operator
                if operator.getSymbol().text == "/":
                    logging.debug("FACTOR: unary / unary")
                    result = TypedValue(result.value / unary_tv.value, float)
                elif operator.getSymbol().text == "*":
                    logging.debug("FACTOR: unary * unary")
                    result = TypedValue(result.value * unary_tv.value, float)
                else:
                    raise AssertionError("found unknown operation inside factor")
            return result

    # Visit a parse tree produced by SPLParser#unary.
    def visitUnary(self, ctx: SPLParser.UnaryContext):
        # checking unary -> primary
        if ctx.primary() is not None:
            return self.visitPrimary(ctx.primary())
        # checking unary -> "not" unary
        elif ctx.NOT() and ctx.unary() is not None:
            unary = self.visitUnary(ctx.unary())
            # 'not' only on boolean
            if unary.inferred_type is bool:
                logging.debug("found unary is 'not' unary")
                return TypedValue(value=not unary.value, inferred_type=bool)
            else:
                semantic_error("NOT can only be used on boolean values", ctx)
        # checking unary -> "minus" unary
        elif ctx.MINUS() and ctx.unary() is not None:
            unary = self.visitUnary(ctx.unary())
            # 'minus' only on float
            if unary.inferred_type is float:
                logging.debug("found unary is Minus unary")
                unary.value = - unary.value
                return TypedValue(value=-unary.value, inferred_type=float)
            else:
                semantic_error("MINUS can only be used on number values", ctx)
        else:
            raise AssertionError("encountered unexpected state in unary")

    # Visit a parse tree produced by SPLParser#primary.
    def visitPrimary(self, ctx: SPLParser.PrimaryContext):
        if ctx.expression() is not None:
            return self.visitExpression(ctx.expression())
        elif ctx.NUMBER() is not None:
            logging.debug(f"found number {float(ctx.getText())} in primary")
            return TypedValue(value=float(ctx.getText()), inferred_type=float)
        elif ctx.TRUE() is not None:
            logging.debug("found true in primary")
            return TypedValue(value=True, inferred_type=bool)
        elif ctx.FALSE() is not None:
            logging.debug("found false in primary")
            return TypedValue(value=False, inferred_type=bool)
        elif ctx.STRING() is not None:
            logging.debug("found string in primary")
            return TypedValue(value=ctx.getText(), inferred_type=str)
        elif ctx.IDENTIFIER() is not None:
            # lookup the current value of the variable with this name
            try:
                lookup_result = self.environment[str(ctx.getText())]

                if lookup_result is None:
                    semantic_error(f"variable '{ctx.getText()}' has not been initialized", ctx)
                else:
                    #logging.debug(f"successful lookup of variable '{ctx.getText()}' "
                    #              f"(= {lookup_result.value} {lookup_result.inferred_type}) in primary")
                    return lookup_result
            except KeyError:
                semantic_error(f"encountered undeclared variable '{ctx.getText()}', ctx")
        else:
            logging.error("encountered unexpected state in primary")
            return None


del SPLParser
