# Definition der Semantik

## Implizite Boolean-Konvertierung
Grundsaetzlich verbieten wir jegliche Art impliziter Konvertierung,
da dies fehleranfaelliger fuer den Nutzer ist.

## Operator Ueberladen
Operatoren werden nur auf zwei Werte desselben Typs angewandt (bis auf  
den gesonderten "Operator" "="). Konvertierung  
ist nicht moeglich.  
  
- für Zahlen greift: "+", "-", "*", "/", "<", ">", "<=", ">=", "= =", "!="  
- Für Strings greift: "+", "= =", "!="  
- Für Boolean greift: "and", "or", "!", "= =", "!="  

## Neudefinition von Variablen
Variablen dürfen innerhalb eines Scopes _nicht_ mehrmals
definiert werden. Beispiel:

    var a = "before"; print a; var a = "after"; print a;
führt zu einem semantischen Fehler.

## Shadowing und Scoping
Die Neudefinition von Variablen in inneren Blöcken (Scopes) _ist erlaubt_. 
Die Definition
von einem Variablen mitdemselben Namen wie in einem äußeren Scope führt
zu _Shadowing_, d.h. die innere Variable ist vollkommen unabhängig von der 
äußeren Variable. Der Gülitgkeitsbereich einer Variablen endet mit dem
Ende des Scopes, in der sie definiert wurde.

## Uninitialisierte Werte

Alle Variablen müssen vor der Nutzung initialisiert werden - standardmäßig
haben sie keinen Wert. Wird eine uninitialisierte Variable verwendet, so entsteht
ein semantischer Fehler.

# Wie stellen wir sicher, dass diese Regeln eingehalten werden?

Die Umsetzung dieser Regeln wird im Visitor (/gen/SPLVisitor.py) umgesetzt, dabei 
* wird die implizite Boolean-Konvertierung unterbunden, indem stets geprüft wird, dass die Datentypen stimmen. Vergleiche Zeile 80, 98 und 340.
* wird die Anwendung von Operatoren nur auf gleiche und passende Datentypen stets geprüft, vergleiche Zeile 142, 164, 188, 205, 241, 283 usw.
* wird die mehrfache Definition von Variablen innerhalb des selben Scopes unterbunden, vergleiche Zeile 66
* wird Shadowing ermöglicht, vergleiche Zeile 125, 130.
* wird der Umgang mit deklarierten aber uninitialisierten Werten wie oben genannt gehandhabt, vergleiche Zeile 59 für die Deklaration und Zeile 385.