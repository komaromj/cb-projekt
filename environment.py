import logging
from collections import ChainMap, namedtuple

# entries in the lookup table
TypedValue = namedtuple('TypedValue', ['value', 'inferred_type'])


class Environment(ChainMap):
    """Extension of ChainMap that implements the desired SPL scoping semantics
        values are stored as TypedValues; undeclared variables yield a None as value
    """

    def update_declared_var(self, key, value):
        # search for the variable, starting from the innermost scope
        for mapping in self.maps:
            if key in mapping:
                old_value = mapping[key]

                if old_value is not None and old_value.inferred_type != value.inferred_type:
                    raise TypeError
                else:
                    mapping[key] = value
                    return

        # couldn't find the key anywhere
        raise KeyError

    def declare_new_variable(self, key, value=None):

        # check if this variable has already been declared inside the inner scope
        if key in self.maps[0].keys():
            raise LookupError
        else:
            self[key] = value
            if not isinstance(value, TypedValue):
                logging.error("UNEXPECTED: wrong type inserted into environment...")

    # should be equivalent to doing env = env.new_child()
    def enter_new_scope(self):
        self.maps.insert(0, {})

    def exit_scope(self):
        self.maps.pop(0)

